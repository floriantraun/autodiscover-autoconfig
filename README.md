E-Mail Autoconfigure
====================

Some E-Mail clients gather configuration information before setting up mail accounts. This project allows to provide clients like Outlook and Thunderbird the proper mail server configuration, so that users can simply enter their email and password to setup a new mail account.

Installation
------------

### xml files

These files have to be put in the exact same folder structure as it is created here, otherwise it won't work unless you change the config.

### Apache webserver

You need an Apache webserver with PHP7 preconfigures. You can then configure your sites like the example files in /etc/apache2/sites-available

### DNS setup

Configure your DNS records as followed:

```
autoconfig			IN	  CNAME		www
autodiscover		IN	  CNAME		www

@					IN	  MX 10		mail.example.at.
@					IN	  TXT		"mailconf=https://autoconfig.example.at/mail/config-v1.1.xml"

_submission._tcp	SRV 10 1 465	mail.example.at.
_imap._tcp			SRV 20 0 143	mail.example.at.
_imaps._tcp			SRV 10 1 993	mail.example.at.
_pop3._tcp			SRV 20 0 110	mail.example.at.
_pop3s._tcp			SRV 10 1 995	mail.example.at.
_autodiscover._tcp	SRV 0 0 443		autodiscover.example.at.
```
